"use strict";
import { t, Text } from "@mimer/text";
import { c, Calculation } from "@mimer/calculation";
import { BattelAction, Effect } from "../../../../game/action";
import Creature from "../../creature";
import Humanoid from "../humanoid";

//by krm, sandkrypare

export default class Lizardman extends Humanoid {
  static creators = ["sandkrypare"];
  static value = 200;

  constructor(name: string, type: string[], creators = Lizardman.creators) {
    super(name, ["Drake"].concat(type), creators);

    this.size = c().value(15).add.dice(0, 20);
    this.condition = c().value(10).add.dice(0, 10);
    this.strength = c(this).property("size").add.dice(0, 10);
    this.flexibility = c().value(10).add.dice(0, 10);
    this.intelligent = c().value(5).add.dice(0, 5);
    this.charisma = c().value(0).add.dice(0, 5);
    this.wisdom = c().value(0).add.dice(0, 10);
    this.might = c().value(20).add.dice(0, 20);
    this.maxHP = c(this)
      .value(75)
      .add.property("size")
      .add.property("condition");
    this.hp = NaN;
    this.cost += 300;

    this._description = t("En stor grön drake");
    this._salute = t("Du kommer aldig besegra den gamla draken");
    this._ask = t("Vad ska vi göra för att besegra honom?");
    this._lose = t("Spring för ditt liv");
    this._win = t("Det var enkelt!");
    this._title = t();
  }

  damage(occasion: Effect): Effect {
    return occasion;
  }

  Scratch(): BattelAction {
    const my_action = new BattelAction(this, "Eldskada", "Träffad av eld");
    const my_occation = my_action.createOccasion();
    const my_effect = my_occation.createEffect();
    const effect_bleed = my_occation.createEffect();

    my_action.tags.push("body");
    my_action.initiativ.dice(1, 10).add.property("condition");
    my_action.description = t("Eldskada, ger mycket skada");

    my_occation.accuracy.value(1).percent.value(80); //0.7 + this.flexibility / 20;

    my_effect.type = "damage";
    my_effect.value.value(24);

    effect_bleed.type = "bleed";
    effect_bleed.value.value(4);
    effect_bleed.duration = c().value(4);

    return my_action;
  }

  Shank(): BattelAction {
    const my_action = new BattelAction(
      this,
      "Svans",
      "genomborade med sin svans"
    );
    const my_occation = my_action.createOccasion();
    const my_effect = my_occation.createEffect();
    my_action.tags.push("steel");
    my_action.initiativ.dice(1, 10).add.property("condition");
    my_action.description = t("Drake skickar hälsningar");

    my_occation.accuracy.value(1).percent.value(80);

    my_effect.type = "damage";
    my_effect.value.dice(1, 10).add.property("strength").add.property("size");

    return my_action;
  }

  UnstablefireBall(): BattelAction {
    const my_action = new BattelAction(
      this,
      "Ostabil eldboll",
      "sprutade en eldboll"
    );
    const my_occation = my_action.createOccasion();
    const my_effect = my_occation.createEffect();
    my_action.tags.push("steel");
    my_action.initiativ.dice(1, 5).add.property("condition");
    my_action.description = t("Detta kommer brännas");

    my_occation.accuracy.value(1).percent.value(20); //this.flexibility / (this.flexibility*(this.strength *0.5));

    my_effect.type = "damage";
    my_effect.value.dice(1, 10).mult.property("might");

    return my_action;
  }

  selection(): BattelAction[] {
    let result = super.selection();
    result.push(this.Scratch());
    result.push(this.Shank());
    result.push(this.UnstablefireBall());
    this.numberOfSelections += 3;
    return result;
  }

  static infoChooseMe() {
    return t("En grön drake");
  }

  static info() {
    return t("En grön drake").blue;
  }
}
