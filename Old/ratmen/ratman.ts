"use strict";
import { t, Text } from "@mimer/text";
import { c, Calculation } from "@mimer/calculation";
import { BattelAction, Effect } from "../../../../game/action";
import Creature from "../../creature";
import Humanoid from "../humanoid";

//by krm, papabubba

export default class Ratman extends Humanoid {
  static creators = ["papabubba"];
  static value = 800;

  constructor(name: string, type: string[], creators = Ratman.creators) {
    super(name, ["Råttman"].concat(type), creators);

    this.size = c().value(2).add.dice(0, 5);
    this.condition = c().value(20).add.dice(0, 8);
    this.strength = c().value(7).add.dice(0, 5);
    this.flexibility = c().value(13).add.dice(0, 6);
    this.intelligent = c().value(3).add.dice(0, 2);
    this.charisma = c().value(0).add.dice(0, 2);
    this.wisdom = c().value(25).add.dice(0, 10);
    this.might = c().value(10).add.dice(0, 10);
    this.maxHP = c(this).value(35).add.property("size").add.property("wisdom");
    this.hp = NaN;
    this.cost += 200;

    this._description = t("En råttman, svag men vis");
    this._salute = t("Gnagar bra och är snabb som blixten!");
    this._ask = t("Hur löser vi detta?");
    this._lose = t("Hur hann du undan mig...");
    this._win = t("Med sådana bett så är det självklart att jag vann.");
    this._title = t();
  }

  Bite(): BattelAction {
    const my_action = new BattelAction(this, "Bett", "biten av råtttänder");
    const my_occation = my_action.createOccasion();
    const my_effect = my_occation.createEffect();
    const effect_bleed = my_occation.createEffect();

    my_action.tags.push("body");
    my_action.initiativ.dice(1, 10).add.property("condition");
    my_action.description = t(
      "Biter, gör medium skada och skapar blödning på den andra"
    );

    my_occation.accuracy.value(1).percent.value(90);

    my_effect.type = "damage";
    my_effect.value.add.property("strength").dice(5, 10);

    effect_bleed.type = "bleed";
    effect_bleed.value.value(5);
    effect_bleed.duration = c().value(3);

    return my_action;
  }

  TailConfusion(): BattelAction {
    const my_action = new BattelAction(
      this,
      "Svansförvirring",
      "förvirrad av råttsvans"
    );
    const my_occation = my_action.createOccasion();
    const my_effect = my_occation.createEffect();

    my_action.tags.push("body");
    my_action.initiativ.dice(1, 10).add.property("flexibility");
    my_action.description = t("Förvirrar med svans, gör fienden förvirrad");

    my_occation.accuracy.value(1).percent.value(70);

    my_effect.type = "confound";
    my_effect.duration = c().value(7);

    return my_action;
  }

  selection(): BattelAction[] {
    let result = super.selection();
    result.push(this.Bite());
    result.push(this.TailConfusion());
    this.numberOfSelections += 2;
    return result;
  }

  static infoChooseMe() {
    return t("Välj en råttman");
  }

  static info() {
    return t("En råttman").blue;
  }
}
