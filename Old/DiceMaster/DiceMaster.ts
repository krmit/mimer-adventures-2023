"use strict";
import { msg } from "../../../../game/content";
import { Effect } from "../../../../game/effect";
import { c, Calculation } from "@mimer/calculation";
import { BattelAction } from "../../../../game/action/battelAction";
import Creature from "../../creature";
import Humanoid from "../humanoid";

//by krm. Sr.Sim_Sala_Bim

export default class Dicemaster extends Humanoid {
  static creators = ["Sr.Sim_Sala_Bim"];
  static value = 500;

  status: number;
  min: number;
  max: number;

  constructor(name: string, type: string[], creators = Dicemaster.creators) {
    super(name, ["Lucky Joe"].concat(type), creators);

    this.min = 1;
    this.max = 4;
    this.status = this.GetRandomInt();

    this.size = c().value(0).add().dice(0, 100);
    this.condition = c().value(0).add().dice(0, 100);
    this.strength = c(this).property("size").add().dice(0, 100);
    this.flexibility = c().value(0).add().dice(0, 100);
    this.intelligent = c().value(0).add().dice(0, 100);
    this.charisma = c().value(0).add().dice(0, 100);
    this.wisdom = c().value(0).add().dice(0, 100);
    this.might = c().value(0).add().dice(0, 100);
    this.maxHP = c(this)
      .value(1)
      .add()
      .property("size")
      .add()
      .property("condition");
    this.hp = NaN;
    this.cost += 500;

    this._description = msg("A little gambler addicted to gambling");
    this._salute = msg("So what's the bet?");
    this._ask = msg("What are we betting on?");
    this._lose = msg("I can't afford this...");
    this._win = msg("How will you pay? debit or credit?");
    this._title = msg();
  }

  damage(occasion: Effect): Effect {
    return occasion;
  }

  GetRandomInt(): number /* Doesnt this run just once and then the effect is the same every time??? */ {
    this.min = Math.ceil(this.min);
    this.max = Math.floor(this.max);
    return Math.floor(Math.random() * (this.max - this.min + 1)) + this.min;
  }

  DiceThrow(): BattelAction {
    const my_action = new BattelAction(this, "Gamble", "Throw the dice");
    const my_occation = my_action.createOccasion();
    const effect_damage = my_occation.createEffect();
    const effect_bleed = my_occation.createEffect();
    const effect_healing = my_occation.createEffect();
    const effect_confound = my_occation.createEffect();

    my_action.tags.push("body");
    my_action.initiativ.dice(1, 10).add().property("condition");
    my_action.description = msg(
      "Let's see what the dice will give, maybe a som damage maybe not"
    );

    my_occation.accuracy.value(1).percent(90);

    if (this.status == 1 /*damage*/) {
      effect_damage.type = "damage";
      effect_damage.value.value(1).dice(0, 5).add().property("strength");
    } else if (this.status == 2 /*bleed*/) {
      effect_bleed.type = "bleed";
      effect_bleed.value.dice(0, 5).add().property("might");
      effect_bleed.duration?.dice(1, 3);
    } else if (this.status == 3 /*healing*/) {
      effect_healing.type = "healing";
      effect_healing.value.dice(0, 5).add().property("intelligent");
    } else if (this.status == 4 /*confound*/) {
      effect_confound.type = "confound";
      effect_confound.duration = c().dice(0, 5);
    }
    return my_action;
  }

  CardTrick(): BattelAction {
    const my_action = new BattelAction(
      this,
      "Card Trick",
      "Show your opponenet a card trick, very magical. Hard to get opponents attention"
    );
    const my_occation = my_action.createOccasion();
    const my_effect = my_occation.createEffect();
    my_action.tags.push("steel");
    my_action.initiativ.dice(1, 10).add().property("condition");
    my_action.description = msg(
      "Lucky Joe pulls up a card deck and the magic begins!!!"
    );

    my_occation.accuracy.value(1).percent(20); // Not posibel for now dice(20, 99);

    my_effect.type = "damage";
    my_effect.value
      .dice(1, 100)
      .add()
      .property("size")
      .add()
      .property("condition")
      .add()
      .property("flexibility")
      .add()
      .property("strength")
      .add()
      .property("intelligent")
      .add()
      .property("charisma")
      .add()
      .property("wisdom")
      .add()
      .property("might");

    return my_action;
  }

  Roulette(): BattelAction {
    const my_action = new BattelAction(
      this,
      "Roulette",
      "All or nothing. Better pray to Gods"
    );
    const my_occation = my_action.createOccasion();
    const my_effect = my_occation.createEffect();
    my_action.tags.push("steel");
    my_action.initiativ.dice(1, 5).add().property("condition");
    my_action.description = msg("What number did you bet on???");

    my_occation.accuracy.value(1).percent(100);

    my_effect.type = "damage";
    my_effect.value.dice(999, 9999);

    return my_action;
  }

  Bluff(): BattelAction {
    const my_action = new BattelAction(
      this,
      "Bluff",
      "See if you can bluff harder in poker than your enemy."
    );
    const my_occation = my_action.createOccasion();
    const my_effect = my_occation.createEffect();

    my_action.tags.push("body");
    my_action.initiativ.dice(1, 10).add().property("condition");
    my_action.description = msg("Maybe a Royal Flush");

    my_occation.accuracy.value(1).percent(80); //dice(80, 90);

    my_effect.type = "damage";
    my_effect.value.dice(1, 10).percent(50); //dice(0, 300);

    return my_action;
  }

  BlackJack(): BattelAction {
    const my_action = new BattelAction(
      this,
      "Black Jack",
      "Play Black Jack and make *Black Jack* do your deeds against the enemy"
    );
    const my_occation = my_action.createOccasion();
    const my_effect = my_occation.createEffect();

    my_action.tags.push("body");
    my_action.initiativ.dice(1, 10).add().property("condition");
    my_action.description = msg("And I've gotten....");

    my_occation.accuracy.value(1).percent(60); //dice(60, 100);

    my_effect.type = "damage";
    my_effect.value.dice(4, 21);

    return my_action;
  }

  selection(): BattelAction[] {
    let result = super.selection();
    result.push(this.DiceThrow());
    result.push(this.CardTrick());
    result.push(this.Roulette());
    result.push(this.Bluff());
    result.push(this.BlackJack());
    this.numberOfSelections += 5;
    return result;
  }

  static infoChooseMe() {
    return msg("Feeling lucky?").red;
  }

  static info() {
    return msg("Lucky Joe").blue;
  }
}
