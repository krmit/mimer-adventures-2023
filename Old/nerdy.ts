"use strict";
import { t, Text } from "@mimer/text";
import { c } from "@mimer/calculation";
import Action from "../../../game/action";

export default function Nerdy(base: any) {
  return class extends base {
    static creators = ["ZaDoS"];
    static value = 1000;

    constructor(...args: any[]) {
      super(args[0] as string);
      this.cost = this.cost * 1.4;
      this.intelligent.mult.value(8);
      this.charisma.div.value(5);
      this.wisdom.mult.value(2);
      this.might.add.value(69);
      this.strength.div.value(7);
      this.maxHP = c(this)
        .value(70)
        .add.property("size")
        .add.property("condition");
    }

    static info(): Text {
      return t("En nördig krigare!").blue;
    }
  };
}
