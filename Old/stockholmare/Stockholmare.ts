"use strict";
import { t, Text } from "@mimer/text";
import { c, Calculation } from "@mimer/calculation";
import { BattelAction } from "../../../../game/action";
import Creature from "../../creature";
import Humanoid from "../humanoid";

export default class Stockholmare extends Humanoid {
  static creators = ["Achtung06"];
  static value = 600;

  constructor(
    name: string,
    type: string[] = [],
    creators = Stockholmare.creators
  ) {
    super(name, ["Stockholmare"].concat(type), creators);
    this.cost = this.cost + Stockholmare.value;
    this.size = c().value(8).add.dice(0, 15);
    this.condition = c().value(15).add.dice(0, 10);
    this.strength = c(this).value(5).add.dice(0, 20);
    this.flexibility = c().value(5).add.dice(0, 10);
    this.intelligent = c().value(0).add.dice(0, 5);
    this.charisma = c().value(5).add.dice(0, 5);
    this.wisdom = c().value(5).add.dice(0, 10);
    this.might = c().value(5).add.dice(0, 10);
    this.maxHP = c(this)
      .value(50)
      .add.property("size")
      .add.property("condition");
    this.hp = NaN;
    this.cost += 600;

    this._description = t(
      "En fjollig stockholmare från Södermalm eller en livsfarlig AIK supporter"
    );
    this._salute = t("Ska vi ta en Latte med sojamjölk?");
    this._ask = t("Macke, kan du ringa åklagarn?");
    this._lose = t("Macke! De bröt min arm!");
    this._win = t("Jag är med i Firman Boys!");
    this._title = t();
  }

  Stolskast(): BattelAction {
    const my_action = new BattelAction(this, "Kasta stol, rätt i ansktet");
    const my_occation = my_action.createOccasion();
    const my_effect = my_occation.createEffect();

    my_action.tags.push("body");
    my_action.initiativ.dice(1, 10).add.property("condition");
    my_action.description = t("Kaste en stol på motståndaren!");

    my_effect.type = "damage";
    my_effect.value.dice(1, 8).add.property("strength/size").add.value(8);

    return my_action;
  }

  selection(): BattelAction[] {
    let result = super.selection();
    result.push(this.Stolskast());
    this.numberOfSelections++;
    return result;
  }

  static infoChooseMe() {
    return t("Välj en fjollig Stockholmare");
  }

  static info() {
    return t("En fjollig stockholmare").blue;
  }
}
