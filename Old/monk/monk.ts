"use strict";
import { t, Text } from "@mimer/text";
import { c } from "@mimer/calculation";
import { BattelAction, Effect } from "../../../game/action";
import Creature from "../creature";
import Human from "./human";

export default class Monk extends Human {
  static creators = ["syltgrodan"];
  static value = 5000;

  constructor(name: string, type: string[] = [], creators = Monk.creators) {
    super(name, ["Monk"].concat(type), creators);
    this.cost = this.cost + Monk.value;
    this.size = c().value(10).add.dice(0, 5);
    this.condition = c().value(15).add.dice(0, 8);
    this.strength = c().value(20).add.dice(0, 10);
    this.flexibility = c().value(15).add.dice(0, 8);
    this.intelligent = c().value(25).add.dice(0, 15);
    this.charisma = c().value(5).add.dice(0, 3);
    this.wisdom = c().value(20).add.dice(0, 10);
    this.might = c().value(5).add.dice(0, 10);
    this.maxHP = c(this)
      .value(80)
      .add.property("size")
      .add.property("condition");
    this.hp = NaN;
    this._description = t("En vis munk som är utbildad i närstrid");
    this._salute = t("Välkommen främling");
    this._lose = t("Vad gick fel!?, jag bör meditera på det här");
    this._ask = t("Vad ska vi äta?");
    this._win = t("Bra kämpat, men vinsten kommer alltid vara min");
    this._title = t();
  }

  meditate(): BattelAction {
    const my_action = new BattelAction(this, "Ett målmat", "ättit");
    const my_occation = my_action.createOccasion();
    const my_effect = my_occation.createEffect();

    my_action.tags.push("body");
    my_action.initiativ.property("flexibility");
    my_action.description = t("Munken sätter sig ner och mediterar");

    my_occation.accuracy.value(1);
    my_occation.type = "self";

    my_effect.type = "healing";
    my_effect.value.dice(0, 10).add.property("strength");

    return my_action;
  }

  selection(): BattelAction[] {
    let result = super.selection();
    result.push(this.meditate());
    this.numberOfSelections++;
    return result;
  }

  static infoChooseMe(): Text {
    return t("Välj en munk").red;
  }

  damage(effect: Effect): Effect {
    if (effect.occasion.action.tags.indexOf("food") !== -1) {
      effect.value.div.value(2);
    }
    return effect;
  }

  static info() {
    return t("En munk som har tränat hela sitt liv för detta").blue;
  }
}
