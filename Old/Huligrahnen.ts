"use strict";
import { t, Text } from "@mimer/text";
import { c } from "@mimer/calculation";
import { BattelAction, Occasion } from "../../../game/action";
import Creature from "../creature";
import Human from "./human";

export default class Huligrahnen extends Human {
  static creators = ["ZaDoS"];
  static value = 5000;

  constructor(
    name: string,
    type: string[] = [],
    creators = Huligrahnen.creators
  ) {
    super(name, ["Huligrahnen"].concat(type), creators);
    this.cost = this.cost + Huligrahnen.value;
    this.size = c().value(5).add.dice(0, 1);
    this.condition = c().value(6).add.dice(0, 4);
    this.strength = c().value(20).add.dice(0, 20);
    this.flexibility = c().value(6);
    this.intelligent = c().value(3);
    this.charisma = c().value(80);
    this.wisdom = c().value(5).add.dice(0, 20);
    this.might = c().value(20);
    this.maxHP = c(this)
      .value(2)
      .add.property("size")
      .add.property("condition");
    this.hp = NaN;
    this._description = t("En mentalt störd MFF supporter!");
    this._salute = t("Våld löser allt!");
    this._lose = t("Domaren luktar whisky!");
    this._ask = t("E de fel i pallet po daj?");
    this._win = t("Vi har fler pedofiler än Hammarby har guld!");
    this._title = t();
  }

  highpunch(): BattelAction {
    const my_action = new BattelAction(
      this,
      "Ett slag mot bollarna!",
      "Hårt slagen!"
    );
    const my_occation = my_action.createOccasion();
    const my_effect = my_occation.createEffect();

    my_action.tags.push("body");
    my_action.initiativ.property("condition");
    my_action.description = t("Huligrahnen slår mot bollarna!");

    my_occation.accuracy.value(1);

    my_effect.type = "damage";
    my_effect.value.value(2000);

    return my_action;
  }

  static infoChooseMe(): Text {
    return t("Välj huligrahnen!").red;
  }

  static info() {
    return t("En vanlig MFF huligan!").blue;
  }
}
