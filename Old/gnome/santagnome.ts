"use strict";
import { t } from "@mimer/text";
import { c } from "@mimer/calculation";
import forestgnome from "./forestgnome";
import { BattelAction } from "../../../../game/action";

export default class santagnome extends forestgnome {
  static creators = ["cowdigger"];
  static value = 350;

  constructor(name: string, type: string[], creators = santagnome.creators) {
    super(name, ["santagnome"].concat(type), creators);
    this.cost = this.cost + santagnome.value;
    this.might = c().add.dice(0, 20);
    this.size = c().value(8);
    this.strength = c().add.dice(0, 10);
    this.charisma = c().add.dice(10, 30);

    this._description = t("En jultomte som ger ut presenter till små barn.");
    this._title = t();
  }

  bopShroom(): BattelAction {
    const my_action = new BattelAction(this, "Ger present", "Present given");
    const my_occation = my_action.createOccasion();
    const my_effect = my_occation.createEffect();

    my_action.tags.push("body");
    my_action.initiativ.dice(2, 9).add.property("condition");
    my_action.description = t("En explosiv present!");

    my_occation.accuracy.add.percent // Ny unik effekt som använder charisma för att se om den träffar motståndaren
      .property("charisma")
      .value(50);

    my_effect.type = "damage";
    my_effect.value.dice(1, 10).add.property("charisma");
    return my_action;
  }

  static infoChooseMe() {
    return t("Välj en jultomte!");
  }

  static info() {
    return t("En tome med långt vitt skägg").blue;
  }
}
