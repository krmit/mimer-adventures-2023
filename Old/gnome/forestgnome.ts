"use strict";
import { t, Text } from "@mimer/text";
import { c, Calculation } from "@mimer/calculation";
import { BattelAction } from "../../../../game/action";
import Creature from "../../creature";
import Humanoid from "../humanoid";

export default class ForestGnome extends Humanoid {
  static creators = ["cowdigger"];
  static value = 350;

  constructor(
    name: string,
    type: string[] = [],
    creators = ForestGnome.creators
  ) {
    super(name, ["ForestGnome"].concat(type), creators);
    this.cost = this.cost + ForestGnome.value;
    this.size = c().value(2).add.dice(0, 2);
    this.condition = c().value(20).add.dice(0, 10);
    this.strength = c(this).value(3).add.dice(0, 15);
    this.flexibility = c().value(30).add.dice(5, 15);
    this.intelligent = c().value(2).add.dice(0, 4);
    this.charisma = c().value(1).add.dice(0, 3);
    this.wisdom = c().value(3).add.dice(1, 2);
    this.might = c().value(50).add.dice(0, 10);
    this.maxHP = c(this)
      .value(25)
      .add.property("size")
      .add.property("condition");
    this.hp = NaN;
    this.cost += 600;

    this._description = t(
      "En liten magisk godhjärtig varelse som försöker bli osedd men den kräver respekt och betalning för arbete."
    );
    this._salute = t("Dags för betalning.");
    this._ask = t("Varför har du inte lagt ut någon gröt?");
    this._lose = t("Detta kommer du att ångra! Tro mig!");
    this._win = t("Du skulle respekterat mig! Din familj är näst på tur.");
    this._title = t();
  }

  bopShroom(): BattelAction {
    const my_action = new BattelAction(this, "Slukar svamp", "Svamp slukad");
    const my_occation = my_action.createOccasion();
    const my_effect = my_occation.createEffect();

    my_action.tags.push("body");
    my_action.initiativ.dice(4, 10).add.property("condition");
    my_action.description = t("En magisk svamp som läker sår!");

    my_effect.type = "healing";
    my_effect.value.value(5);
    return my_action;
  }

  selection(): BattelAction[] {
    let result = super.selection();
    result.push(this.bopShroom());
    this.numberOfSelections++;
    return result;
  }

  static infoChooseMe() {
    return t("Välj en skogstomte");
  }

  static info() {
    return t("En skoggstomte").blue;
  }
}
