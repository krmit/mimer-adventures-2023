"use strict";
import { msg } from "../../../src/lib/msg";
import { Occasion } from "../../../src/occasion/occasion";
import { BattelAction } from "../../../src/action/battelAction";
import Humanoid from "../../creatures/humanoids/humanoid";

export default function BigBrain(base: any) {
  return class BigBrain extends base {
    static creators = ["miljonmannen"];
    static value = 300;

    constructor(...args: any[]) {
      super(args[0] as string);

      /* Lägg till egenskaperna som BigBrain har på den klassen som mix-inas.  */
      this.cost += BigBrain.value;
      this.size.mult.value(1.05);
      this.intelligent.mult.value(3);
      this.wisdom.mult.value(3);
      this.maxHP.add().property("intelligent");

      /* Ändra titel och beskrivning */
      this._title = msg("Big brain ", this._title);
      this._description = msg(
        "En smart som är " + this.description,
        "Gör skada genom att påverka magnetfältet på motståndarens hjärna."
      );

      /* Ändra de olika replikerna som typen säger. */
      this._salute = msg("Ingen kan är lika smart som jag!");
      this._lose = msg("Kunskap vinner ändå alltid i längden!");
      this._win = msg("Kunskapens kraft är oövervinnlig!");
      this._title = msg("En smart varelse med en stor hjärna");
    }
    /* En attack som skapar ett magnetiskt fält och påvkerar motståndarens hjärnkapacitet, därmed tar motståndaren skada */
    magneticField(): BattelAction {
      /* Skapa en ny battle action */
      const newAction = new BattelAction(
        this as unknown as Humanoid,
        "Magnetfältsattack",
        "skadad av magnetfältets kraft"
      );
      /* Skapa en ny händelse och en effekt är damage till det.*/
      const newOccation = newAction.createOccasion();
      const newEffect = newOccation.createEffect();

      newAction.tags.push("body");
      newAction.description = msg("Skadad genom magnetfältets kraft");

      newOccation.accuracy.value(1).percent(85);

      newEffect.type = "damage";
      newEffect.value.dice(1, 5).add().value(10).add().property("intelligent");

      return newAction;
    }

    selection(): BattelAction[] {
      let result = super.selection();
      /* Lägg till magneticField-attacken */
      result.push(this.magneticField());
      this.numberOfSelections++;
      return result;
    }

    damage(action: Occasion): Occasion {
      let result = super.damage(action);
      return result;
    }
    static info(): string {
      return msg("Innerhar en extra stor hjärna!");
    }
  };
}
