"use strict";
import { msg } from "../../../src/lib/msg";
import { c } from "@mimer/calculation";
import Humanoid from "../../creatures/humanoids/humanoid";

export default function Strong(base: any) {
  return class Strong extends base {
    static creators = ["timce"];
    static value = 500;

    constructor(...args: any[]) {
      super(args[0] as string);
      this.cost *= 0.5;
      this.size.mult.value(2);
      this.condition.mult.value(2);
      this.strength.mult.value(2);
      this.flexibility.mult.value(2);
      this.intelligent.mult.value(2);
      this.charisma.mult.value(2);
      this.wisdom.mult.value(2);
      this.might.mult.value(2);
      this.maxHP = c(this)
        .value(50)
        .add()
        .property("size")
        .add()
        .property("condition");
    }

    static info(): string {
      return msg("En stark krigare!");
    }
  };
}
