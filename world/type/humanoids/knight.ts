"use strict";
import { msg } from "../../../src/lib/msg";
import { c } from "@mimer/calculation";
import { BattelAction } from "../../../src/action/battelAction";
import { Occasion } from "../../../src/occasion/occasion";
import Humanoid from "../../creatures/humanoids/humanoid";

export default function Knight(base: any) {
  return class Knight extends base {
    static creators = ["timce"];
    static value = 500;

    constructor(...args: any[]) {
      super(args[0] as string);
      this.cost += Knight.value;
      this._description = msg(
        "Riddaren är en mäktig krigare. ",
        "Ridaren gör stor skada genom att hugga med sitt ",
        "svärd. Rustningen skyddar mot olika attacker."
      );
      this._salute = msg("Jag ska rädda dig ifrån detta hemska monster! 😇");
      this._lose = msg("Ack och ve, onskan vinner igen i denna onda värld!");
      this._win = msg("Rättvisan har segrat!");
      this._title = msg("Riddare");
    }

    cut(): BattelAction {
      const my_action = new BattelAction(
        this as unknown as Humanoid,
        "Svärdshugg",
        "huggen med svärd"
      );
      const my_occation = my_action.createOccasion();
      const my_effect = my_occation.createEffect();

      my_action.tags.push("steel");
      my_action.initiativ.property("flexibility").add().value(2);
      my_action.description = msg("Huggen med svärd");

      my_occation.accuracy.value(1).percent(90);

      my_effect.type = "damage";
      my_effect.value.dice(1, 10).add().value(2).mult().property("strength");

      return my_action;
    }

    selection(): BattelAction[] {
      let result = super.selection();
      result.push(this.cut());
      this.numberOfSelections++;
      return result;
    }

    damage(action: Occasion): Occasion {
      let result = super.damage(action);
      result.damage.sub.value(10);
      return result;
    }

    static info(): string {
      return msg("En bra krigare!");
    }
  };
}
