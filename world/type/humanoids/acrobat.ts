"use strict";
import { msg } from "../../../src/lib/msg";
import { c } from "@mimer/calculation";
import Humanoid from "../../creatures/humanoids/humanoid";

export default function Acrobat(base: any) {
  return class Acrobat extends base {
    static creators = ["cowdigger"];
    static value = 500;

    constructor(...args: any[]) {
      super(args[0] as string);
      this.cost *= 1.1;
      this.condition.mult.value(2);
      this.flexibility.mult.value(4);
      this.intelligent.mult.value(2);
      this.strength.div.value(4);
      this.maxHP.div.value(2);
    }

    static info(): string {
      return msg("En smidig akrobat!");
    }
  };
}
