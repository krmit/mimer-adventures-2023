"use strict";
import { msg } from "../../../src/lib/msg";
import { c } from "@mimer/calculation";
import Humanoid from "../../creatures/humanoids/humanoid";

export default function Wise(base: any) {
  return class Wise extends base {
    static creators = ["papabubba"];
    static value = 500;

    constructor(...args: any[]) {
      super(args[0] as string);
      this.cost *= 0.75;
      this.flexibility.mult.value(1.5);
      this.intelligent.add().value(10);
      this.charisma.div.value(4);
      this.wisdom.mult.value(4);
      this._title = msg("Vis", this._title);
    }

    static info(): string {
      return msg("En vis och flexibel krigare!");
    }
  };
}
