"use strict";
import { msg } from "../../../src/lib/msg";
import { BattelAction } from "../../../src/action/battelAction";
import { Occasion } from "../../../src/occasion/occasion";
import Humanoid from "../../creatures/humanoids/humanoid";

// timce

export default function DeadKnight(base: any) {
  return class DeadKnight extends base {
    static creators = ["timce"];
    static value = 500;

    constructor(...args: any[]) {
      super(args[0] as string);
      this.cost += DeadKnight.value;
      this._description = msg("En odöd krigare ");
      this._salute = msg("Ni kommer alla gå med mig förr eller senare");
      this._lose = msg("Jag kommer att återvända!");
      this._ask = msg("Omae wa mou shindeiru?");
      this._win = msg("Den Odöda armen växer!");
      this._title = msg("Odöd Riddare");
    }

    cut(): BattelAction {
      const my_action = new BattelAction(
        this as unknown as Humanoid,
        "Svärdshugg",
        "huggen med svärd"
      );
      const my_occation = my_action.createOccasion();
      const my_effect = my_occation.createEffect();

      my_action.tags.push("steel");
      my_action.initiativ.property("flexibility").add().value(2);
      my_action.description = msg("Huggen med svärd");

      my_occation.accuracy.value(1).percent(90);

      my_effect.type = "damage";
      my_effect.value.dice(1, 10).add().value(20).mult().property("strength");

      return my_action;
    }

    selection(): BattelAction[] {
      let result = super.selection();
      result.push(this.cut());
      this.numberOfSelections++;
      return result;
    }

    damage(action: Occasion): Occasion {
      let result = super.damage(action);
      result.damage.sub.value(20);
      return result;
    }

    static info(): string {
      return msg("En Odöd Death Knight.");
    }
  };
}
