"use strict";
import { msg } from "../../../src/lib/msg";
import { c } from "@mimer/calculation";
import Humanoid from "../../creatures/humanoids/humanoid";

export default function Weak(base: any) {
  return class Weak extends base {
    static creators = ["krm"];
    static value = -500;

    constructor(...args: any[]) {
      super(args[0] as string);
      this.cost *= 0.5;
      this.size.div.value(2);
      this.condition.div.value(2);
      this.strength.div.value(2);
      this.flexibility.div.value(2);
      this.intelligent.div.value(2);
      this.charisma.div.value(2);
      this.wisdom.div.value(2);
      this.might.div.value(2);
      this.maxHP = c(this)
        .value(50)
        .add()
        .property("size")
        .add()
        .property("condition");
      this._title = msg("Fattig ", this._title);
    }

    static info(): string {
      return msg("En fattig krigare!");
    }
  };
}
