"use strict";
import { msg } from "../../../src/lib/msg";
import { c } from "@mimer/calculation";
import Humanoid from "../../creatures/humanoids/humanoid";

export default function Stupid(base: any) {
  return class extends base {
    static creators = ["xd"];
    static value = 500;

    constructor(...args: any[]) {
      super(args[0] as string);
      this.cost = this.cost * 0.75;
      this.intelligent.div.value(10);
      this.charisma.div.value(2);
      this.wisdom.div.value(4);
      this.might.mult.value(0);
    }

    static info(): string {
      return msg("En stark men korkad krigare!").blue;
    }
  };
}
