"use strict";
/**
 * The Calculation class represent calculation.
 */
export default abstract class Resource {
  static creators = ["krm"];
  static value = 0;
  name: string;
  cost: number;

  constructor(name: string) {
    this.name = name;
    this.cost = Resource.value;
  }
}
