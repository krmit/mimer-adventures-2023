"use strict";
import Creature from "./creatures/creature";
import Humanoid from "./creatures/humanoids/humanoid";
import Lizardman from "./creatures/humanoids/lizardmen/lizardman";
import Dwarf from "./creatures/humanoids/dwarf/dwarf";
import Longbeards from "./creatures/humanoids/dwarf/longbeards";
import Halflings from "./creatures/humanoids/halflings/halfling";
//import Dicemaster from "./creature/humanoids/DiceMaster/DiceMaster";
import Human from "./creatures/humanoids/human";
import Skaning from "./creatures/humanoids/skåning";
import Knight from "./type/humanoids/knight";
import Strong from "./type/humanoids/strong";
import Weak from "./type/humanoids/weak";
import Stupid from "./type/humanoids/stupid";
import DeathKnight from "./type/humanoids/deathKnight";
import BigBrain from "./type/humanoids/bigBrain";
import Acrobat from "./type/humanoids/acrobat";
import FireGiant from "./creatures/humanoids/giant/firegiant";
import Werewolf from "./creatures/humanoids/werewolf/werewolf";
import Elf from "./creatures/humanoids/elf/elf";
import Shapeshifterwerewolf from "./creatures/humanoids/werewolf/shapeshifterwerewolf";
//import Firman from "./creatures/humanoids/stockholmare/Firman";
//import Huligrahnen from "./creatures/humanoids/Huligrahnen";
//import Nerdy from "./type/humanoids/nerdy";
//import Forestgnome from "./creatures/humanoids/gnome/forestgnome";
//import Santagnome from "./creatures/humanoids/gnome/santagnome";
//import Monk from "./creatures/humanoids/monk/monk";
//import Ratman from "./creatures/humanoids/ratmen/ratman";
import Wise from "./type/humanoids/wise";

interface worldInterface {
  creatures: { [key: string]: any };
  types: { [key: string]: any };
}

let world: worldInterface = {
  creatures: {
    Human: Human,
    Lizardman: Lizardman,
    //    DiceMaster: Dicemaster,
    Dwarf: Dwarf,
    Longbeards: Longbeards,
    Firegiant: FireGiant,
    Skåning: Skaning,
    Halflings: Halflings,
    Werewolf: Werewolf,
    Shapeshifterwerewolf: Shapeshifterwerewolf,
    Elf: Elf
    // Huligrahnen: Huligrahnen,
    // Forestgnome: Forestgnome,
    //  Monk: Monk,
    //  Ratman: Ratman
  },
  types: {
    knight: Knight,
    weak: Weak,
    strong: Strong,
    deadKnight: DeathKnight,
    stupid: Stupid,
    bigBrain: BigBrain,
    //    firman: Firman,
    //    nerdy: Nerdy,
    acrobat: Acrobat,
    wise: Wise,
  },
};

export default world;
