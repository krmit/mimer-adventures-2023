"use strict";
import { msg } from "../../../src/lib/msg";
import { c } from "@mimer/calculation";
import Human from "./human";
import { BattelAction } from "../../../src/action/battelAction";
import { Effect } from "../../../src/effect/effect";

export default class Skaning extends Human {
  static creators = ["krm"];
  static value = 5000;

  constructor(name: string, type: string[] = [], creators = Skaning.creators) {
    super(name, ["Skåning"].concat(type), creators);
    this.cost = this.cost + Skaning.value;
    this.size = c().value(15).add().dice(0, 5);
    this.condition = c().value(5).add().dice(0, 10);
    this.strength = c().value(10).add().dice(0, 20);
    this.flexibility = c().value(5).add().dice(0, 10);
    this.intelligent = c().value(20).add().dice(0, 10);
    this.charisma = c().value(20).add().dice(0, 10);
    this.wisdom = c().value(20).add().dice(0, 10);
    this.might = c().value(5).add().dice(0, 10);
    this.maxHP = c(this)
      .value(100)
      .add()
      .property("size")
      .add()
      .property("condition");
    this.hp = NaN;
    this._description = msg("En helt vanlig skåning!");
    this._salute = msg("Jag kan äta!");
    this._lose = msg("nä!");
    this._ask = msg("Vad ska vi äta?");
    this._win = msg("Kan jag äta mitt pris?");
    this._title = msg();
  }

  eat(): BattelAction {
    const my_action = new BattelAction(this, "Ett målmat", "ättit");
    const my_occation = my_action.createOccasion();
    const my_effect = my_occation.createEffect();

    my_action.tags.push("body");
    my_action.initiativ.property("flexibility");
    my_action.description = msg("Skåningen äter ett mål mat.");

    my_occation.accuracy.value(1);
    my_occation.type = "self";

    my_effect.type = "healing";
    my_effect.value.dice(0, 10).add().property("size");

    return my_action;
  }

  selection(): BattelAction[] {
    let result = super.selection();
    result.push(this.eat());
    this.numberOfSelections++;
    return result;
  }

  static infoChooseMe(): string {
    return msg("Välj en vanlig skåning!");
  }

  damage(effect: Effect): Effect {
    if (effect.occasion.action.tags.indexOf("food") !== -1) {
      effect.value.div().value(2);
    }
    return effect;
  }

  static info() {
    return msg("En vanlig mäniska från skåneland!").blue;
  }
}
