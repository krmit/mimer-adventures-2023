"use strict";
import { c, Calculation } from "@mimer/calculation";
import { msg, msgnl, bold, labelAuthors } from "../../../src/lib/msg";
import { Effect } from "../../../src/effect/effect";
import Creature from "../creature";

export default abstract class Humanoid extends Creature {
  static creators = ["krm"];
  static value = 0;
  size: Calculation = c();
  condition: Calculation = c();
  strength: Calculation = c();
  flexibility: Calculation = c();
  intelligent: Calculation = c();
  charisma: Calculation = c();
  wisdom: Calculation = c();
  might: Calculation = c();
  private _protection = 0;
  private _armor = 0;

  constructor(name: string, type: string[], creators = Humanoid.creators) {
    super(name, ["humanoid", ...type]);
    this.cost = this.cost + Creature.value;
  }

  showStats() {
    return msg(
      this.showBanner,
      "\n",
      "Cost        ",
      String(this.cost),
      " 💰",
      "\n",
      "HP          ",
      String(this.hp),
      " ❤️",
      "\n",
      "Max HP      ",
      String(this.maxHP.result()),
      " ❤️",
      "\n",
      "Size        ",
      String(this.size.result()),
      " 🔺",
      "\n",
      "Strength    ",
      String(this.strength.result()),
      " 💪",
      "\n",
      "Condition   ",
      String(this.condition.result()),
      " 🏃‍",
      "\n",
      "Flexibility ",
      String(this.flexibility.result()),
      " 💃",
      "\n",
      "Intelligent ",
      String(this.intelligent.result()),
      " ♟",
      "\n",
      "Charisma    ",
      String(this.charisma.result()),
      " 🗣 ",
      "\n",
      "Wisdom      ",
      String(this.wisdom.result()),
      " 📚",
      "\n",
      "Might       ",
      String(this.might.result()),
      " ❗️",
      "\n\n",
      this.labelDescription,
      "\n"
    );
  }

  showCalculation() {
    const _this = this;

    return msg(
      this.showBanner,
      "\n",
      "Max HP      ",
      this.maxHP.toString(),
      " ❤️",
      "\n",
      "Size        ",
      this.size.toString(),
      " 🔺",
      "\n",
      "Strength    ",
      this.strength.toString(),
      " 💪",
      "\n",
      "Condition   ",
      this.condition.toString(),
      " 🏃‍",
      "\n",
      "Flexibility ",
      this.flexibility.toString(),
      " 💃",
      "\n",
      "Intelligent ",
      this.intelligent.toString(),
      " ♟",
      "\n",
      "Charisma    ",
      this.charisma.toString(),
      " 🗣 ",
      "\n",
      "Wisdom      ",
      this.wisdom.toString(),
      " 📚",
      "\n",
      "Might       ",
      this.might.toString(),
      " ❗️",
      "\n"
    );
  }

  damage(action: Effect): Effect {
    return action;
  }

  roll() {
    this.size.roll();
    this.condition.roll();
    this.strength.roll();
    this.flexibility.roll();
    this.intelligent.roll();
    this.charisma.roll();
    this.wisdom.roll();
    this.might.roll();
    this.maxHP.roll();
    this.hp = this.maxHP.result();
  }
}
