"use strict";
import { msg } from "../../../../src/lib/msg";
import Werewolf from "./werewolf";
import Human from "../human";

export default class Shapeshifterwerewolf extends Werewolf {
  static creators = ["miljonmannen"];
  static value = 400;

  constructor(name: string, type: string[], creators = Werewolf.creators) {
    super(name, ["Isig"].concat(type), creators);
    this.cost = this.cost + Werewolf.value;

    // Skapar en "exempel"- human för att dess konstruktor ska sätta upp egenskaperna efter human, och sedan använd dessa egenskaperna i skapandet av en människoliknande varulv.
    const originalHuman = new Human("Shapeshifter", []); // Ny funktionalitet som inte finns implementerad tidigare

    // Lägg till egenskaperna, intelligens & vishet från originalHuman.
    this.intelligent = originalHuman.intelligent;
    this.wisdom = originalHuman.wisdom;

    this._description = msg(
      "En människa som kan förändras till varulv vid eget behov, därav har den kvar de mänskliga egenskaperna så som intellekt och kunskap."
    );
    this._title = "";
  }

  static infoChooseMe() {
    return msg("Välj en intelligent varulv!");
  }

  static info() {
    return msg("En människa som varulv!");
  }
}
