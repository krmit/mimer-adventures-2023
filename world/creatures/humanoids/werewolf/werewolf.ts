"use strict";
import { msg, msgnl, bold, labelAuthors } from "../../../../src/lib/msg";
import { c } from "@mimer/calculation";
import { BattelAction } from "../../../../src/action/battelAction";
import { Effect } from "../../../../src/effect/effect";
import Humanoid from "../humanoid";

//by miljonmannen

export default class Werewolf extends Humanoid {
  static creators = ["miljonmannen"];
  static value = 300;

  constructor(name: string, type: string[], creators = Werewolf.creators) {
    super(name, ["Varulv"].concat(type), creators);

    this.cost += Werewolf.value;

    /* Sätt olika egenskaper efter hur en varulv förväntas vara, dvs flexibel, stark och har bra kondition, däremot inte bra vishet eller intellekt. */
    this.size = c(10).add().dice(0, 8);
    this.condition = c(30).add().dice(0, 10);
    this.strength = c(this).property("size").add().dice(0, 10);
    this.flexibility = c(25).add().dice(5, 10);
    this.intelligent = c(2).add().dice(0, 4);
    this.charisma = c(0).add().dice(0, 5);
    this.wisdom = c(0).add().dice(0, 5);
    this.might = c(15).add().dice(0, 5);
    this.maxHP = c(this)
      .value(85)
      .add()
      .property("condition")
      .add()
      .property("strength");
    this.hp = NaN;

    /* Olika repliker för olika tillfällen i spelet */
    this._description = msg("En varulv");
    this._salute = msg("Auhhh du kommer aldrig kunna besegra nattens ledare!");
    this._ask = msg("Vad ska vi göra nu?");
    this._lose = msg("Grrrr, jag tar dig nästa gång!");
    this._win = msg("Precis som väntat!");
    this._title = "";
  }

  damage(occasion: Effect): Effect {
    return occasion;
  }

  Bite(): BattelAction {
    /* Skapa den nya händelsen */
    const newAction = new BattelAction(
      this,
      "Bita",
      "biten av varulvens starka tänder"
    );
    /* Skapa effekter för händelsen */
    const newOccation = newAction.createOccasion();
    const damageEffect = newOccation.createEffect();
    const bleedEffect = newOccation.createEffect();

    newAction.tags.push("body");
    newAction.initiativ.dice(3, 8).add().property("condition");
    newAction.description = msg(
      "Biter, gör skada och motståndaren börjar blöda"
    );

    newOccation.accuracy.value(1).percent(90);

    /* Damage-effekten är instant och beror på hur stark varulven är  */
    damageEffect.type = "damage";
    damageEffect.value = c(this).add().property("strength").div().value(2);

    /* Blödningseffekten varar i 2 rundor och gör 5 skada i varje runda. */
    bleedEffect.type = "bleed";
    bleedEffect.value.value(5);
    bleedEffect.duration = c().value(2);

    return newAction;
  }

  Shake(): BattelAction {
    /* Skapa den nya battle-händelsen */
    const newAction = new BattelAction(
      this,
      "Skaka",
      "tagen i munnen och skakad av varulvens starka tänder"
    );
    /* Skapa effekter kopplade till händelsen */
    const newOccation = newAction.createOccasion();
    const damageEffect = newOccation.createEffect();

    newAction.tags.push("body");
    newAction.initiativ.dice(2, 6).add().property("condition");
    newAction.description = msg(
      "Tar motståndaren i munnen och skakar aggresivt"
    );

    newOccation.accuracy.value(1).percent(70);

    /* Effekten som händelsen gör är av typen skada, värdet beror på styrkan och storleken på varulven. */
    damageEffect.type = "damage";
    damageEffect.value = c(this)
      .value(2)
      .add()
      .property("strength")
      .div()
      .value(2)
      .add()
      .property("size");

    return newAction;
  }
  LeapAttack(): BattelAction {
    /* Skapa nya battle-händelser */
    const newAction = new BattelAction(this, "Hoppa", "påhoppad av varulven");
    const newOccation = newAction.createOccasion();
    const damageEffect = newOccation.createEffect();

    newAction.tags.push("body");
    newAction.initiativ.dice(3, 8).add().property("condition");
    newAction.description = msg("Hoppar på motståndaren och attackerar den");

    newOccation.accuracy.value(1).percent(80);

    /* Attacken är av typen "damage", beror på styrkan och flexibiliteten hos varulven.  */
    damageEffect.type = "damage";
    damageEffect.value = c(this)
      .add()
      .property("strength")
      .div()
      .value(3)
      .add()
      .property("flexibility")
      .div()
      .value(3);

    return newAction;
  }

  selection(): BattelAction[] {
    let result = super.selection();

    /* Registrera de olika attackerna */
    result.push(this.Bite());
    result.push(this.Shake());
    result.push(this.LeapAttack());

    this.numberOfSelections++;
    return result;
  }

  static infoChooseMe() {
    return msg("En varulv som kan bitas hårt!").blue;
  }

  static info() {
    return msg("En varulv").blue;
  }
}
