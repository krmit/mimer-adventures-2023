"use strict";
import { msg } from "../../../../src/lib/msg";
import { c } from "@mimer/calculation";
import { BattelAction } from "../../../../src/action/battelAction";
import Creature from "../../creature";
import Humanoid from "../humanoid";

export default class Halfling extends Humanoid {
  static creators = ["blytinator"];
  static value = 300;

  constructor(name: string, type: string[], creators = Halfling.creators) {
    super(name, ["Halvling"].concat(type), creators);
    this.size = c().value(3).add().dice(0, 6);
    this.condition = c().value(9).add().dice(0, 10);
    this.strength = c().value(3).add().dice(0, 4);
    this.flexibility = c().value(8).add().dice(0, 11);
    this.intelligent = c().value(12).add().dice(5, 12);
    this.charisma = c().value(9).add().dice(0, 14);
    this.wisdom = c().value(7).add().dice(0, 10);
    this.might = c().value(3).add().dice(0, 7);
    this.maxHP = c(this)
      .value(40)
      .add()
      .property("wisdom")
      .add()
      .property("condition");
    this.hp = NaN;
    this.cost += 300;

    this._description = msg(
      "En halv människa, inte så stark eller stor men mycket smart och smidig."
    );
    this._salute = msg(
      "Jag kan kanske inte slåss så bra men jag kan tänka till"
    );
    this._ask = msg("Hur ska jag göra nu?");
    this._lose = msg("Jag behöver kanske tänka om min strategi...");
    this._win = msg("Ytterliggare en vinst!");
    this._title = msg();
  }

  magicTrick(): BattelAction {
    const my_action = new BattelAction(
      this,
      "Utför magitrick",
      "Magiskt förbluffad"
    );
    const my_occation = my_action.createOccasion();
    const my_effect = my_occation.createEffect();

    my_action.tags.push("body");
    my_action.initiativ.dice(1, 10).add().property("condition");
    my_action.description = msg("Ett trolleritrick som förbluffar fienden");

    my_occation.accuracy.value(0.5).add().dice(0, 0.5).percent(70);

    my_effect.type = "confound";
    my_effect.duration = c().value(3);

    return my_action;
  }

  selection(): BattelAction[] {
    let result = super.selection();
    result.push(this.magicTrick());
    this.numberOfSelections++;
    return result;
  }

  static infoChooseMe() {
    return msg("Välj en halvling");
  }

  static info() {
    return msg("En vanlig halvling").blue;
  }
}
