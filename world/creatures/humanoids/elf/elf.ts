import Humanoid from "../humanoid";
import { BattelAction } from "../../../../src/action/battelAction";
import { Effect } from "../../../../src/effect/effect";
import { c } from "@mimer/calculation";

export default class Elf extends Humanoid {
  static creators = ["krm"];
  static value = 350;

  constructor(name: string, type: string[] = [], creators = Elf.creators) {
    super(name, ["Mäniska", ...type], creators);
    this.cost = this.cost + Elf.value;

    // Abilitys

    this.size.value(10).add().dice(0, 10);
    this.condition.value(10).add().dice(0, 10);
    this.strength.value(10).add().dice(0, 10);
    this.flexibility.value(15).add().dice(0, 10);
    this.intelligent.value(15).add().dice(0, 10);
    this.charisma.value(15).add().dice(0, 10);
    this.wisdom.value(10).add().dice(0, 10);
    this.might.value(15).add().dice(0, 10);
    this.maxHP = c(this)
      .value(50)
      .add()
      .property("size")
      .add()
      .property("condition")
      .add()
      .dice(0, 10);

    // Data

    this.hp = NaN;

    // Description

    this._description = "En helt vanlig Alv.";
    this._salute = "Var ärad min finde!";
    this._lose = "Detta var inte rätt!";
    this._ask = "Vad är ädlast att göra nu?";
    this._win = "Jag viste att vi skulle vinna?";
    this._title = "";
  }

  hit(): BattelAction {
    // Inizsiate objects.
    const hit_action = new BattelAction(this, "Ett knytnävesslag", "slagen");
    const my_occation = hit_action.createOccasion();
    const my_effect = my_occation.createEffect();

    // Action
    hit_action.tags.push("body");
    hit_action.initiativ.value(this.flexibility.result()).mult().value(2);
    hit_action.description =
      "Ett knytnävesslag, ger inte mycket skada men går snabbt";
    my_occation.accuracy.value(1).percent(80);

    my_effect.type = "damage";
    my_effect.value.dice(1, 10).add(this.strength.result());

    return hit_action;
  }

  selection(): BattelAction[] {
    let result = super.selection();
    result.push(this.hit());
    this.numberOfSelections++;
    return result;
  }

  damage(effect: Effect): Effect {
    effect.value.mult(0.9);
    return effect;
  }

  static infoChooseMe(): string {
    return "Välj en vanlig Alv!";
  }

  static info() {
    return "En vanlig Alv!";
  }
}
