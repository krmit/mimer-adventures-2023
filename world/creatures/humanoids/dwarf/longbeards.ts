"use strict";
import { msg } from "../../../../src/lib/msg";
import { c } from "@mimer/calculation";
import Dwarf from "./dwarf";

export default class Longbeards extends Dwarf {
  static creators = ["krm"];
  static value = 200;

  constructor(name: string, type: string[], creators = Longbeards.creators) {
    super(name, ["Långskägg"].concat(type), creators);
    this.cost = this.cost + Longbeards.value;
    this.wisdom = c().value(12).add().dice(0, 20);
    this.might = c().value(10).add().dice(0, 20);
    this.cost += 200;

    this._description = msg(
      "En liten varelse som ser ut som en människa och bor i hålor och har långt skägg"
    );
    this._title = msg();
  }

  static infoChooseMe() {
    return msg("Välj en dvärg med långt skägg!");
  }

  static info() {
    return msg("En Dvärg med långt skägg").blue;
  }
}
