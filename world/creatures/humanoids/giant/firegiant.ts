"use strict";
import { msg } from "../../../../src/lib/msg";
import { c } from "@mimer/calculation";
import Giant from "./gigant";
import { BattelAction } from "../../../../src/action/battelAction";

export default class FireGiant extends Giant {
  static creators = ["miljonmannen"];
  static value = 280;

  constructor(name: string, type: string[], creators = FireGiant.creators) {
    super(name, ["Eld"].concat(type), creators);
    this.cost = this.cost + FireGiant.value; // Kostnaden för en FireGiant ska läggas till på den totala kostnaden.
    this.strength = c().value(super.strength.result()).add().dice(0, 10);
    this.intelligent = c().value(super.intelligent.result()).sub().dice(0, 10);

    this._description = msg(
      super._description + " och har brännande hårda slag"
    );
    this._title = msg();
  }
  heavyFireSmash(): BattelAction {
    const newAction = new BattelAction(
      this,
      "Slår hårt och ger motståndaren brännskador",
      "slagen hårt och fick brännskador"
    );
    // Skapa nya händelser och effekter
    const newOccation = newAction.createOccasion();
    const damageEffect = newOccation.createEffect(); // Damage effekt
    const fireEffect = newOccation.createEffect(); // Burn effekt.

    newAction.tags.push("body"); // Targeta kroppen
    newAction.initiativ.dice(1, 3).add().property("flexibility");
    newAction.description = msg("Ett kraftfull slag som ger brännskador!");

    damageEffect.type = "damage"; // Effekten är av typ "damage" vilket gör att attacken bara skadar motståndaren
    damageEffect.value.dice(5, 25).add().property("size").add().value(8); //Attackens damage beror på storleken på jätten.

    fireEffect.type = "burn"; // Effekten är av typ "damage" vilket gör att attacken bara skadar motståndaren
    fireEffect.value.dice(2, 6); //Attackens damage beror på storleken på jätten.
    fireEffect.duration = c().value(2).add().dice(0, 3); // Effekten ska vara max 5 rundor, men minst 2.

    return newAction;
  }
  selection(): BattelAction[] {
    let result = super.selection();
    result.push(this.heavyFireSmash());
    this.numberOfSelections++;
    return result;
  }
  static infoChooseMe() {
    return msg("Välje en jätte som har brännande hårda slag!");
  }

  static info() {
    return msg("En jätte som har lärt sig eld.").blue;
  }
}
