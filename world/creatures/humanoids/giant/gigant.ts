"use strict";
import { msg } from "../../../../src/lib/msg";
import { c, Calculation } from "@mimer/calculation";
import { BattelAction } from "../../../../src/action/battelAction";
import Creature from "../../creature";
import Humanoid from "../humanoid";

export default class Giant extends Humanoid {
  static creators = ["SunKen"];
  static value = 600;

  constructor(name: string, type: string[] = [], creators = Giant.creators) {
    super(name, ["Jätte"].concat(type), creators);
    this.cost = this.cost + Giant.value;
    this.size = c().value(20).add().dice(0, 40);
    this.condition = c().value(20).add().dice(0, 10);
    this.strength = c(this).value(20).add().dice(0, 10);
    this.flexibility = c().value(5).add().dice(0, 10);
    this.intelligent = c().value(1).add().dice(0, 9);
    this.charisma = c().value(1).add().dice(0, 9);
    this.wisdom = c().value(10).add().dice(0, 20);
    this.might = c().value(1).add().dice(0, 9);
    this.maxHP = c(this)
      .value(100)
      .add()
      .property("size")
      .add()
      .property("condition");
    this.hp = NaN;

    this._description = msg(
      "En stor människoliknande varelse som bor i stora djupa grottor"
    );
    this._salute = msg("Lika stor och stark som en borg");
    this._ask = msg("Vad ska jag göra?");
    this._lose = msg("Hur är detta möjlig att någon lyckats klå mig?!?");
    this._win = msg("Styrka vinner alltid över allt annat!");
    this._title = msg();
  }

  heavySmash(): BattelAction {
    const my_action = new BattelAction(
      this,
      "Slår jätte hårt",
      "jätte hårt slagen"
    );
    const my_occation = my_action.createOccasion();
    const my_effect = my_occation.createEffect();

    my_action.tags.push("body");
    my_action.initiativ.dice(1, 5).add().property("flexibility");
    my_action.description = msg("Ett riktigt hårt slag!");

    my_effect.type = "damage";
    my_effect.value.dice(1, 20).add().property("size").add().value(5);

    return my_action;
  }

  selection(): BattelAction[] {
    let result = super.selection();
    result.push(this.heavySmash());
    this.numberOfSelections++;
    return result;
  }

  static infoChooseMe() {
    return msg("Välj en Jätte");
  }

  static info() {
    return msg("En Jätte").blue;
  }
}
