"use strict";
import { c } from "@mimer/calculation";
import { BattelAction } from "../../../src/action/battelAction";
import { Effect } from "../../../src/effect/effect";
import Humanoid from "./humanoid";

export default class Human extends Humanoid {
  static creators = ["krm"];
  static value = 250;

  constructor(name: string, type: string[] = [], creators = Human.creators) {
    super(name, ["Mäniska", ...type], creators);
    this.cost = this.cost + Human.value;
    this.size = c(10).add().dice(0, 10);
    this.condition = c(10).add().dice(0, 10);
    this.strength = c(10).add().dice(0, 10);
    this.flexibility = c(10).add().dice(0, 10);
    this.intelligent = c(10).add().dice(0, 10);
    this.charisma = c(10).add().dice(0, 10);
    this.wisdom = c(10).add().dice(0, 10);
    this.might = c(10).add().dice(0, 10);
    this.maxHP = c(this)
      .value(50)
      .add()
      .property("size")
      .add()
      .property("condition")
      .add()
      .dice(0, 10);
    this.hp = NaN;
    this._description = "En helt vanlig mäniska.";
    this._salute = "Jag kan inte slås!";
    this._lose = "nej!";
    this._ask = "Vad ska vi göra?";
    this._win = "Hur är detta möjligt?";
    this._title = "";
  }

  hit(): BattelAction {
    const my_action = new BattelAction(this, "Ett knytnävesslag", "slagen");
    const my_occation = my_action.createOccasion();
    const my_effect = my_occation.createEffect();

    my_action.tags.push("body");
    my_action.initiativ.value(this.flexibility.result()).mult().value(2);
    my_action.description =
      "Ett knytnävesslag, ger inte mycket skada men går snabbt";
    my_occation.accuracy.value(1).percent(80);

    my_effect.type = "damage";
    my_effect.value.dice(1, 10).add(this.strength.result());

    return my_action;
  }

  selection(): BattelAction[] {
    let result = super.selection();
    result.push(this.hit());
    this.numberOfSelections++;
    return result;
  }

  damage(effect: Effect): Effect {
    return effect;
  }

  static infoChooseMe(): string {
    return "Välj en vanlig mäniska!";
  }

  static info() {
    return "En vanlig mäniska!";
  }
}
