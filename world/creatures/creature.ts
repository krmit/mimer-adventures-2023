"use strict";
import { c, Calculation } from "@mimer/calculation";
import Resource from "../resource";
import { BattelAction } from "../../src/action/battelAction";
import { msg, msgnl, bold, labelAuthors } from "../../src/lib/msg";
import { Effect } from "../../src/effect/effect";
import { Occasion } from "../../src/occasion/occasion";

/**
 * The Calculation class represent calculation.
 */
export default abstract class Creature extends Resource {
  static creators = ["krm"];
  static value = 0;
  hp = 0;
  maxHP: Calculation = c();
  type: string[];
  numberOfSelections = 0;
  protected _title: string = "";
  protected _description: string = "";
  protected _salute: string = "";
  protected _ask: string = "";
  protected _lose: string = "";
  protected _win: string = "";

  constructor(name: string, type: string[]) {
    super(name);
    this.cost = this.cost + Creature.value;
    this.type = type;
  }

  get labelName(): string {
    if (this._title !== "") {
      return this._title + " " + this.name;
    } else {
      return this.name;
    }
  }

  get labelType(): string {
    return this.type.join("/");
  }

  get labelDescription(): string {
    return this._description;
  }

  get sayName(): string {
    return this.labelName + ": ";
  }

  get saySalute(): string {
    return this.sayName + this._salute;
  }

  get sayQuestion(): string {
    return this.sayName + this._ask;
  }

  get sayWin(): string {
    return this.sayName + this._win;
  }

  get sayLose(): string {
    return this.sayName + this._lose;
  }

  get showTitel(): string {
    return "# " + this.labelName;
  }

  get showBanner(): string {
    return msg(this.showTitel, this.labelType, "\n");
  }

  get showStatus(): string {
    if (this.isFighting()) {
      return msg(
        this.sayName,
        bold(String(this.hp)),
        "/",
        String(this.maxHP.result),
        " ❤️"
      );
    } else {
      return msg(this.sayName, "🙁");
    }
  }

  static showChooseMe(): string {
    return "No info";
  }

  static createRandom(): any {
    return null;
  }

  isFighting(): boolean {
    return this.hp > 0;
  }

  selection(): BattelAction[] {
    this.numberOfSelections = 0;
    return [];
  }

  do(occasion: Occasion): string {
    let roll = c().dice(1, 100).roll().result() / 100.0;
    occasion.roll();
    let result = "";
    if (roll < occasion.accuracy.result()) {
      let effect = occasion.effect[0];
      effect.roll();
      result += msgnl(
        occasion.showDamageLabel,
        "Aj, jag tog ",
        String(effect.value.result()),
        " ❤️  i skada. Chansen var ",
        String(occasion.accuracy.result + "%"),
        "🎯"
      );
      this.hp -= effect.value.result();
      if (!this.isFighting()) {
        result += this.sayLose;
        result += occasion.action.by.sayWin;
      }
    } else {
      result += msgnl(
        occasion.showDamageLabel,
        "Ha! Du missade! Chansen var ",
        String(occasion.accuracy.result) + "%",
        "🎯"
      );
    }
    return result;
  }

  abstract showStats(): string;

  abstract damage(action: Effect): Effect;

  abstract roll(action: void): void;
}
