"use strict";
import { msg } from "../lib/msg";
import Player from "../player";
import { Config, GameStatus } from "./game";
import AdventureGame from "./game";
import Mode from "../mode/mode";
import Start from "../mode/start";
import End from "../mode/end";
import Battel from "../mode/battel";

import Creature from "../../world/creatures/creature";
import ChoosePath from "../mode/choosePath";

/**
 * To play a part of the Game quick and simpel.
 */
export default class QuickGame extends AdventureGame {
  mode: Mode;

  constructor(players: Player[], config?: Config) {
    super(players, config);
    this.mode = new Start(players);
  }

  start() {
    return "Start Game!";
  }

  next(player: Player, answer: { [key: string]: number | string }): string {
    let result = this.mode.turn(player, answer);
    if (result === "") {
      if (this.mode.name === "start") {
        this.mode = new ChoosePath(this.players);
      } else if (this.mode.name === "ChoosePath") {
        this.mode = new Battel(this.players);
      } else if (this.mode.name === "battel") {
        this.mode = new End(this.players);
      } else {
        return "end";
      }
    }

    return "next";
  }
}
