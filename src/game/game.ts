"use strict";
import { msg } from "../lib/msg";
import Player from "../player";
export type GameStatus = "start" | "next" | "end" | "error";

export interface Config {
  name?: string;
}

/**
 * The  Game class creating a game engine for logic.
 */
export default abstract class AdventureGame {
  name: string;
  players: Player[];
  status: GameStatus = "start";

  constructor(players: Player[], config: Config = {}) {
    this.name = config.name ?? "";
    this.players = players;
  }

  abstract next(
    player: Player,
    answer: { [key: string]: number | string }
  ): string;
}
