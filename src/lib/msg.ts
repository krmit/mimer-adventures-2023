export function labelAuthors(base: any): string {
  return "Skapat av __" + base.creators.join(", ");
}

export function msgnl(...msgs: string[]): string {
  return msg(...msgs) + "\n";
}

export function msg(...msgs: string[]): string {
  return msgs.join(" ");
}

export function bold(...msgs: string[]): string {
  return "**" + msg(...msgs) + "**";
}

export function headline(...msgs: string[]): string {
  return "#" + msg(...msgs);
}
