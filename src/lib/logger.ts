/*
 *   Copyright 2019 Magnus Kronnäs
 *
 *   This file is part of easy-web-framework.
 *
 *   logger.js is free software: you can redistribute it and/or modify it under the
 *   terms of the GNU General Public License as published by the Free Software
 *   Foundation, either version 3 of the License, or (at your option) any later
 *    version.
 *
 *   easy-web-framework is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License along with
 *   easy-web-framework. If not, see http://www.gnu.org/licenses/.
 */
"use strict";

import colors from "colors";
import moment from "moment";
import yargs from "yargs";

import useragent from "useragent";
import fs from "fs-extra";
import path from "path";
import schedule from "node-schedule";

// Some problem has been observed with jsome, but no cause is knowe
import { getColoredString as pretty } from "jsome";
//const pretty = function(obj){return JSON.stringify(obj, null, 2)};

// Advise from https://www.npmjs.com/package/sleep
function msleep(n: number) {
  Atomics.wait(new Int32Array(new SharedArrayBuffer(4)), 0, 0, n);
}

function sleep(n: number) {
  msleep(n * 1000);
}

const pause = 3;

/**
 * The Logger class that can create a log object.
 */
export default class Logger {
  private logPath: string;

  /**
   * Create a Logger
   *
   */
  constructor() {
    this.logPath = "";
    return this;
  }

  /**
   * Sets a yarg argument
   *
   * @param yarg A yarg object
   */
  yarg(yarg: any) {
    return yarg
      .describe("v", "Give a verbose print")
      .alias("v", "verbose")
      .default("v", false)
      .describe("d", "Give a debug print")
      .alias("d", "debug")
      .default("d", false)
      .describe("t", "Give a trace print")
      .alias("t", "trace")
      .default("t", false)
      .help("h")
      .alias("h", "help")
      .boolean(["d", "v", "t", "h"]);
  }

  logFile(logDir: string) {
    this.logPath = path.resolve(logDir);
    return this;
  }

  log(arg: any): any {
    const l: any = {};

    let level = 5;
    if (arg.verbose) {
      level = 5;
    } else if (arg.debug) {
      level = 6;
    } else if (arg.trace) {
      level = 7;
    }

    let log_file: string;
    let log_dir = this.logPath;
    if (this.logPath) {
      log_file = `${log_dir}/${moment().format("YYYY-MM-DD")}.log`;
      schedule.scheduleJob("0 0 * * *", function () {
        log_file = `${log_dir}/${moment().format("YYYY-MM-DD")}.log`;
      });
    }
    const log_print = function (tag: string, msg: string) {
      let tag_color = function (msg: string) {
        return msg;
      };
      let msg_color = function (msg: string) {
        return msg;
      };
      let console_metod = console.log;
      switch (tag) {
        case "fatal":
          tag_color = colors.red.bold;
          msg_color = colors.bold;
          console_metod = console.error;
          break;
        case "error":
          tag_color = colors.red;
          console_metod = console.error;
          break;
        case "warn":
          tag_color = colors.yellow;
          console_metod = console.warn;
          break;
        case "info":
          tag_color = colors.green;
          console_metod = console.info;
          break;
        case "debug":
          tag_color = colors.green;
          console_metod = console.debug;
          break;
        case "trace":
          tag_color = colors.green;
          console_metod = console.debug;
          break;
        default:
          tag_color = colors.green;
      }

      if (
        typeof msg === "string" ||
        typeof msg === "boolean" ||
        typeof msg === "number"
      ) {
        console_metod(
          "[" +
            tag_color(tag) +
            "]" +
            " ".repeat(6 - tag.length) +
            msg_color(msg)
        );
      } else {
        // Needed good pretty printing of json and object because of circular objects.
        console.log("[" + tag_color(tag) + "]\n" + msg_color(pretty(msg)));
      }
    };

    l.trace = function (msg: string) {};
    if (level > 6) {
      l.trace = function (msg: string) {
        log_print("trace", msg);
      };
    }

    l.debug = function (msg: string) {};
    if (level > 5) {
      l.debug = function (msg: string) {
        log_print("debug", msg);
      };
    }

    l.info = function (msg: string) {};
    if (level > 4) {
      l.info = function (msg: string) {
        log_print("info", msg);
      };
    }

    l.log = function (msg: string) {};
    if (level > 3) {
      l.log = function (msg: string) {
        console.log(msg);
      };
    }

    l.warn = function (msg: string) {};
    if (level > 2) {
      l.warn = function (msg: string) {
        log_print("warn", msg);
      };
    }

    l.error = function (msg: string) {};
    if (level > 1) {
      l.error = function (msg: string) {
        log_print("error", msg);
      };
    }

    l.fatal = function (msg: string) {};
    if (level > 0) {
      l.fatal = function (msg: string) {
        log_print("fatal", msg);
      };
    }

    //Bad solution, better to give standar log and then have a view program if needed.
    l.web = function (req: any) {
      const agent = useragent.parse(req.headers["user-agent"]);
      l.log(
        ("[" + moment().format("YYYY-MM-DD HH:mm:ss") + "] ").green +
          req.ip.bold +
          " " +
          req.protocol.red +
          " " +
          req.method.green +
          " " +
          req.originalUrl.yellow +
          " " +
          agent.toAgent().green +
          " " +
          agent.os.toString().bold +
          " " +
          agent.device.toString().yellow
      );

      if (log_file) {
        fs.appendFile(
          log_file,
          "[" +
            moment().format("YYYY-MM-DD HH:mm:ss") +
            "] " +
            req.ip +
            " " +
            req.protocol +
            " " +
            req.method +
            " " +
            req.originalUrl +
            " " +
            agent.toAgent() +
            " " +
            agent.os.toString() +
            " " +
            agent.device.toString() +
            "\n"
        );
      }
    };

    l.headline = function (msg: string) {};
    if (level > 3) {
      l.headline = function (msg: string) {
        console.log("\n" + colors.bold.underline(msg) + "\n");
      };
    }

    l.code = function (msg: string) {};
    if (level > 3) {
      l.code = function (msg: string) {
        console.log(colors.yellow(msg));
      };
    }

    l.important = function (msg: string) {};
    if (level > 3) {
      l.important = function (msg: string) {
        console.log("\n" + colors.red.bold(msg));
      };
    }

    l.confirm = function () {};
    if (level > 3) {
      l.confirm = function () {
        console.log(colors.yellow.bold("\nIs this rigth? Ctrl-C to cancel."));
        sleep(pause);
      };

      return l;
    }
  }
}
