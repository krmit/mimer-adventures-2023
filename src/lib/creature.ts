import Creature from "../../world/creatures/creature";
import world from "../../world/world";

export function creaturePath(path: string): Creature {
  const creature_class_list = path.split("/");
  let result: Creature;
  result = world.creatures[creature_class_list[0]];
  if (result === undefined) {
    console.error("This creature dose not exsist.");
  }
  for (let i = 1; i < creature_class_list.length; i++) {
    result = world.types[creature_class_list[i]](result);
  }
  return result;
}
