"use strict";
import { msg, headline, bold } from "../lib/msg";
import { c, Calculation } from "@mimer/calculation";
import Player from "../player";
import Mode from "./mode";
import { Config } from "../game/game";

import world from "../../world/world";
import Creature from "../../world/creatures/creature";

function values(o: any) {
  return Object.keys(o).map((key) => o[key]);
}

type statusType = "creature" | "type" | "create" | "end";

/**
 * The  Game class creting a game engine for logic.
 */
export default class SelectCreature extends Mode {
  status: statusType = "creature";
  creature?: any; // class of creature
  creatureName: string = "Adam";

  constructor(players: Player[], config?: Config) {
    super(players, config);
  }

  setNameOnCreatue(name: string) {
    this.creatureName = name;
  }

  turn(player: Player, command: { prompt: number }) {
    let result = "";

    console.log(this.status);
    switch (this.status) {
      case "creature":
        for (const creature of values(world.creatures)) {
          result += creature.infoChooseMe() + "\n";
        }
        this.status = "type";
        this.msgTurn = msg("Vilken varelse vill du välja?");
        break;
      case "type":
        this.creature = values(world.creatures)[command.prompt - 1];
        for (const type of values(world.types)) {
          result += type(this.creature).info();
        }
        this.status = "create";
        this.msgTurn = msg("Vilken type vill du välja?");
        break;
      case "create":
        this.creature = values(world.types)[command.prompt - 1](this.creature);
        this.status = "end";
        this.msgTurn = msg("Creating a character", "\n");
        break;
    }

    return "active";
  }
}
