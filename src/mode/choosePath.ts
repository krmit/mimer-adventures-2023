"use strict";
import { msg, headline, bold } from "../lib/msg";
import { c, Calculation } from "@mimer/calculation";
import Player from "../player";
import Mode from "./mode";
import { Config } from "../game/game";

import world from "../../world/world";
import Creature from "../../world/creatures/creature";

type statusType = "creature" | "name" | "path" | "end";

/**
 * The  Mode class creting a game engine for logic.
 */
export default class ChoosePath extends Mode {
  status: statusType = "creature";
  creature?: any; // class of creature
  creatureName: string = "Adam";

  constructor(players: Player[], config?: Config) {
    super(players, config);
    this.name = "ChoosePath";
  }

  setNameOnCreatue(name: string) {
    this.creatureName = name;
  }

  turn(player: Player, command: { path: string }) {
    let result = "";

    return result;
  }
}
