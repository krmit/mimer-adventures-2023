"use strict";
import { msg, bold } from "../lib/msg";
import Player from "../player";
import { Config } from "../game/game";

export default abstract class Mode {
  name = "";
  players: Player[];
  msgTurn: string = "";
  round = 0;

  constructor(players: Player[], config: Config = {}) {
    this.name = config.name ?? "";
    this.players = players;
  }

  abstract turn(
    player: Player,
    command: { [key: string]: number | string }
  ): string;

  private endRound() {
    this.round++;
    return this.msgTurn;
  }
}
