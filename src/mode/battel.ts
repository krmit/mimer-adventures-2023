"use strict";
import { msg, headline, bold } from "../lib/msg";
import Player from "../player";
import { BattelAction } from "../action/battelAction";
import Mode from "./mode";
import { Config } from "../game/game";

function values(o: any) {
  return Object.keys(o).map((key) => o[key]);
}

export type statusType = "start" | "active" | "prolog" | "end";

/**
 * The  Game class creting a game engine for logic.
 */
export default class Battel extends Mode {
  status: statusType = "start";
  actions: BattelAction[] = [];

  constructor(players: Player[], config?: Config) {
    super(players, config);
  }

  turn(player: Player, command: { prompt: number }): string {
    let result = "";
    switch (this.status) {
      case "start":
        this.status = "active";
        result = bold("Battel begin!");
        break;
      case "active":
        this.status = "prolog";
        result = headline("Turn: ", String(this.turn), "\n");
        break;
      case "prolog":
        this.status = "end";
        result = headline("Battel end!");
        break;
    }
    return result;
  }
}
