"use strict";
import { msg, headline, bold } from "../lib/msg";
import Mode from "./mode";
import Player from "../player";
import { Config } from "../game/game";

type startModeStatus = "active" | "end";

/**
 * The  Game class creting a game engine for logic.
 */
export default class Start extends Mode {
  status: startModeStatus = "active";

  constructor(players: Player[], config?: Config) {
    super(players, config);
  }

  turn(player: Player, command = {}): startModeStatus {
    // Create message
    let result = headline("🔥 Startar spel Mimer Äventyr 🔥 🤠\n");

    // Send messages to all players
    for (const player of this.players) {
      player.msg += result;
    }

    return "end";
  }
}
