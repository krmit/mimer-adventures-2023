import { Calculation, c } from "@mimer/calculation";
import Creature from "../../world/creatures/creature";
import { Action, InputType } from "./action";
import { Occasion } from "../occasion/occasion";

/**
 * Represents a action in a battel
 *
 * @typeParam T Could be an interface for adding meta infromation about an action.
 *
 */
export class BattelAction<T = {}> extends Action {
  initiativ: Calculation;
  occasions: Occasion<T>[] = [];
  input?: InputType<T>[];

  /**
   * The constructor for a BattelAction
   *
   * @param by the creature doing the action.
   * @param name the of the action.
   * @param verb a verb descriping the action.
   */
  constructor(by: Creature, name = "", verb = "") {
    super(by, name, verb);
    this.initiativ = c();
  }

  /**
   * Create occasion relatet to this Action
   */
  createOccasion(): Occasion<T> {
    const result = new Occasion<T>(this);
    this.occasions.push(result);
    return result;
  }
}
