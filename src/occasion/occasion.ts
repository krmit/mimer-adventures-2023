import { Calculation, c } from "@mimer/calculation";
import Creature from "../../world/creatures/creature";
import { BattelAction } from "../action/battelAction";
import { msgnl } from "../lib/msg";
import { Effect } from "../effect/effect";

/**
 * Represents that some things i occuring in the battel.
 */
export class Occasion<T = {}> {
  action: BattelAction<T>;
  target?: Creature;
  accuracy: Calculation;
  effect: Effect<T>[] = [];
  type: "enemy" | "self" = "enemy";
  quantity: number = 1;
  data?: T;

  constructor(action: BattelAction<T>, data: T | undefined = undefined) {
    this.action = action;
    this.accuracy = c();
    this.data = data;
  }

  /**
   * Create an effect on this Occasion
   */
  createEffect(): Effect<T> {
    const result = new Effect<T>(this);
    this.effect.push(result);
    return result;
  }

  /**
   * Make any dice roll for this Occasion.
   */
  roll() {
    this.accuracy.roll();
  }

  /**
   * Make a short text describing the damage.
   *
   * @return a text describing the damage.
   */
  get showDamageLabel(): string {
    return msgnl(
      this.target!.labelName,
      " blir ",
      this.action.verb,
      " av ",
      this.action.by.labelName,
      ": "
    );
  }

  /**
   * Return a text of the title.
   *
   * @return a text of the title.
   */
  get showTitel(): String {
    return this.action.showName;
  }
}
