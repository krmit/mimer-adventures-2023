import { Calculation, c } from "@mimer/calculation";
import { msg } from "../lib/msg";
import { Occasion } from "../occasion/occasion";

/**
 * Represents the effect of a action in a battel.
 */
export class Effect<T = {}> {
  occasion: Occasion<T>;
  type: "damage" | "healing" | "bleed" | "confound" | "burn";
  factors: ("normal" | "fire")[] = [];
  value: Calculation;
  duration?: Calculation;

  constructor(occasion: Occasion<T>) {
    this.occasion = occasion;
    this.type = "damage";
    this.value = c();
  }

  /**
   * Make any dice roll for this Effect.
   */
  roll() {
    this.value.roll();
    if (this.duration !== undefined) {
      this.duration.roll();
    }
  }

  /**
   * Make a short text with infromation about the effect.
   *
   * @return a text information about the effect.
   */
  get showInfo(): string {
    return msg(
      this.occasion.action.showLabel,
      "⚔️ : ",
      this.value.toString(), // this.value.showRange(),
      "🎯: ",
      this.occasion.accuracy.result() + "%",
      " ",
      "⏳: ",
      this.occasion.action.initiativ.toString() // this.occasion.action.initiativ.showRange()
    );
  }

  /**
   * Make a short text describing the effect.
   *
   * @return a text describing the effect.
   */
  get showDescription(): string {
    return msg(
      this.occasion.showTitel + "\n",
      this.occasion.action.description + "\n\n",
      msg("⚔️ : ", this.value.toString()) + "\n",
      msg("🎯: ", this.occasion.accuracy.toString()) + "\n",
      msg("⏳: ", this.occasion.action.initiativ.toString()) + "\n"
    );
  }
}
