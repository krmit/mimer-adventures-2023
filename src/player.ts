"use strict";
import world from "../world/world";
import Creature from "../world/creatures/creature";
import { msg } from "./lib/msg";

export function parseCreature(pathCreature: string) {
  const creature_class_list = pathCreature.split("/");

  let result: Creature;
  result = world.creatures[creature_class_list[0]];
  for (let i = 1; i < creature_class_list.length; i++) {
    result = world.types[creature_class_list[i]](result);
  }
  return result;
}

type playerStatus = "start" | "next" | "end" | "error";

export default class Player {
  name = "";
  id = 1;
  team: Creature[] = [];
  msg: string = "";

  constructor(name = "") {
    this.name = name;
    this.msg = "";
  }

  addCreature(creature: Creature) {
    this.team.push(creature);
  }
}
