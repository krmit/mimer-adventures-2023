# Mimer Äventry

Ett lättsamt rollspel på svenska i en absurd fantasivärld.

I första hand tänkt som en programmeringsövning.

**Not completed yet**

## Install

```
$ npm install
```

## Usage

```
$ mag info
```

## Changelog

- **0.2.0** _2023-02-24_ Remove old code for a new aprouche.
- **0.1.0** _2022-03-24_ Published first version, but it dose not work.

## License

AGPLv3, see COPYING file

## Author

**© 2015-2023 [Magnus Kronnäs](https://magnus.kronnas.se)**
