"use strict";
import { msg } from "../src/lib/msg";
import { Calculation } from "@mimer/calculation";
import Game from "../src/game/quickGame";
import Player from "../src/player";
import { BattelAction } from "../src/action/battelAction";
import world from "../world/world";
import Creature from "../world/creatures/creature";
import Logger from "../src/lib/logger";
import yargs from "yargs";
import Battel from "../src/mode/battel";
import { creaturePath } from "../src/lib/creature";

const logger = new Logger();
let print = true;
export async function aview(yarg: any) {
  const args = yargs
    .command("info [pathCreature]", "start the server", (y) => {
      y.positional("pathCreature", {
        describe: "class and mixins",
        default: "Human",
      });
    })
    .command("roll [pathCreature]", "start the server", (y) => {
      y.positional("pathCreature", {
        describe: "class and mixins",
        default: "Human",
      });
    })
    .command("stats [pathCreature]", "start the server", (y) => {
      y.positional("pathCreature", {
        describe: "class and mixins",
        default: "Human",
      });
    })
    .command("calculation [pathCreature]", "start the server", (y) => {
      y.positional("pathCreature", {
        describe: "class and mixins",
        default: "Human",
      });
    })
    .command("selections [pathCreature]", "start the server", (y) => {
      y.positional("pathCreature", {
        describe: "class and mixins",
        default: "Human",
      });
    })
    .command("action [pathCreature] [selection]", "start the server", (y) => {
      y.positional("pathCreature", {
        describe: "class and mixins",
        default: "Human",
      });
      y.positional("selection", { describe: "class and mixins", default: 0 });
    })
    .command(
      "damage [pathCreature] [selection] [pathCreatureOpponent]",
      "start the server",
      (y) => {
        y.positional("pathCreature", {
          describe: "class and mixins",
          default: "Human",
        });
        y.positional("pathCreatureOpponent", {
          describe: "class and mixins",
          default: "Human",
        });
        y.positional("selection", { describe: "class and mixins", default: 0 });
      }
    )
    .command(
      "turn [pathCreature] [selection] [pathCreatureOpponent] [selectionOpponent]",
      "start the server",
      (y) => {
        y.positional("pathCreature", {
          describe: "class and mixins",
          default: "Human",
        });
        y.positional("pathCreatureOpponent", {
          describe: "class and mixins",
          default: "Human",
        });
        y.positional("selection", { describe: "class and mixins", default: 0 });
        y.positional("selectionOpponent", {
          describe: "class and mixins",
          default: 0,
        });
      }
    )
    .command(
      "battel [pathCreature] [pathCreatureOpponent]",
      "start the server",
      (y) => {
        y.positional("pathCreature", {
          describe: "class and mixins",
          default: "Human",
        });
        y.positional("pathCreatureOpponent", {
          describe: "class and mixins",
          default: "Human",
        });
      }
    )
    .command(
      "battelStats [numberOfBattels] [pathCreature] [pathCreatureOpponent]",
      "start the server",
      (y) => {
        y.positional("pathCreature", {
          describe: "class and mixins",
          default: "Human",
        });
        y.positional("pathCreatureOpponent", {
          describe: "class and mixins",
          default: "Human",
        });
        y.positional("numberOfBattels", {
          describe: "class and mixins",
          default: 10,
        });
      }
    );

  args
    .describe("s", "Give a text as seed for random numbers")
    .alias("s", "seed")
    .default("s", "");
  const argv = logger.yarg(args).argv;
  const log = logger.log(argv);
  log.trace(argv);

  let result: string;

  const command = argv._[0];
  log.trace(command);

  let character: any;
  let opponent: any;
  let character_class: any;
  let opponent_class: any;
  let action: any;
  let game: any;
  let selection = 0;
  let selectionOpponent = 0;
  let last_report: any;
  let numberOfBattels = 1;

  if (argv.pathCreature) {
    character_class = creaturePath(argv.pathCreature);
  }
  if (argv.pathCreatureOpponent) {
    opponent_class = creaturePath(argv.pathCreatureOpponent);
  }
  if (argv.selection) {
    selection = Number(argv.selection) - 1;
  }
  if (argv.selectionOpponent) {
    selectionOpponent = Number(argv.selectionOpponent) - 1;
  }
  if (argv.seed) {
    Calculation.setSeed(argv.seed);
  }
  if (argv.numberOfBattels) {
    numberOfBattels = argv.numberOfBattels;
  }
  switch (command) {
    case "info":
      result = character_class.info();
      break;
    case "roll":
      character = new character_class("Adam");
      character.roll();
      result = msg("No errors?");
      break;
    case "stats":
      character = new character_class("Adam");
      character.roll();
      result = character.showStats();
      break;
    case "calculation":
      character = new character_class("Adam");
      character.roll();
      result = character.showCalculation();
      break;
    case "selections":
      character = new character_class("Adam");
      character.roll();
      let index = 0;
      result = msg(
        character.showBanner,
        msg(
          ...character.selection().map((item: BattelAction) => {
            let effect = item.occasions[0].effect[0];
            effect.roll();
            index++;
            return "\n" + index + ". " + effect.showInfo;
          })
        )
      );
      //result = character.showCalculation();
      break;
    case "action":
      character = new character_class("Adam");
      character.roll();
      let effect = character.selection()[selection].occasions[0].effect[0];
      effect.roll();
      result = effect.showDescription;
      break;
    case "damage":
      character = new character_class("Adam");
      opponent = new opponent_class("Bertil");
      character.roll();
      opponent.roll();
      let character_action = character.selection()[0];
      let character_effect = character_action.occasions[0].effect[0];
      character_effect.roll();
      console.log("!!!");
      result = opponent.damage(character_effect).showDescription;
      break;
    case "start":
      {
        result = msg("# Turn Test for a player 'One'");
        let player_one = new Player("One");
        let player_two = new Player("Two");

        let s = new Battel([player_one, player_two]);
        s.turn(player_one, { prompt: NaN });
      }
      break;
    case "next":
      result = msg("Turn Test for a player 'One'");
      let player_one = new Player("One");
      let player_two = new Player("Two");
      game = new Game([player_one, player_two]);

      game.createRandomCharater(0, "Adam", argv.pathCreature);
      game.createRandomCharater(1, "Bertil", argv.pathCreatureOpponent);
      game.setMode("Battel");
      game.next(0, {
        selection: selection,
        targetCharater: 1,
      });
      result += player_one.msg;
      result += player_two.msg;
      break;
    /*  case "battel":
    result = msg(t("Game Test for a player 'One'").headline);
    game = new Game();
    game.addPlayer("One");
    game.addPlayer("Two");
    let player_one_charater = game.createRandomCharater(
      "One",
      "Adam",
      argv.pathCreature
    );
    let player_two_charater = game.createRandomCharater(
      "Two",
      "Bertil",
      argv.pathCreatureOpponent
    );
    game.setMode("Battel");
    game.setPhase("Turn");
    result.add(game.gameStart());
    last_report = { One: msg(), Two: msg() };
    while (
      !last_report["One"].hasTag("GameOver") &&
      !last_report["Two"].hasTag("GameOver")
    ) {
      let player_one_selection =
        Math.floor(Math.random() * player_one_charater.numberOfSelections) + 1;
      let player_two_selection =
        Math.floor(Math.random() * player_two_charater.numberOfSelections) + 1;
      game.turnCommand("One", {
        characterId: 0,
        selection: player_one_selection - 1,
        targetPlayer: "Two",
        targetId: 0
      });
      game.turnCommand("Two", {
        characterId: 0,
        selection: player_two_selection - 1,
        targetPlayer: "One",
        targetId: 0
      });
      last_report = game.phaseEnd();
      result.add(last_report["One"]);
    }
    if (!last_report["One"].hasTag("GameOver")) {
      result.add(t("Spelare 'One' vann 😀").bold.green);
    } else {
      result.add(t("Spelare 'One' förlorade 🙁").bold.red);
    }
    break;
  case "battelStats":
    result = msg(t("Game Stats for a player 'One'").headline);
    let win = 0;
    let lost = 0;
    let turn_sum = 0;
    let turn_max = 0;
    let turn_min = 10000;
    console.log("\nBattels:");
    for (let i = 0; i < numberOfBattels; i++) {
      game = new Game();
      game.addPlayer("One");
      game.addPlayer("Two");
      let player_one_charater = game.createRandomCharater(
        "One",
        "Adam",
        argv.pathCreature
      );
      let player_two_charater = game.createRandomCharater(
        "Two",
        "Bertil",
        argv.pathCreatureOpponent
      );
      game.setMode("Battel");
      game.setPhase("Turn");
      game.gameStart();
      last_report = { One: msg(), Two: msg() };
      while (
        !last_report["One"].hasTag("GameOver") &&
        !last_report["Two"].hasTag("GameOver")
      ) {
        let player_one_selection =
          Math.floor(Math.random() * player_one_charater.numberOfSelections) +
          1;
        let player_two_selection =
          Math.floor(Math.random() * player_two_charater.numberOfSelections) +
          1;
        game.turnCommand("One", {
          characterId: 0,
          selection: player_one_selection - 1,
          targetPlayer: "Two",
          targetId: 0
        });
        game.turnCommand("Two", {
          characterId: 0,
          selection: player_two_selection - 1,
          targetPlayer: "One",
          targetId: 0
        });
        last_report = game.phaseEnd();
      }
      if (i % 64 === 0) {
        process.stdout.write("\n");
      }
      if (!last_report["One"].hasTag("GameOver")) {
        win++;
        process.stdout.write("😀");
      } else {
        lost++;
        process.stdout.write("🙁");
      }

      turn_sum += game.turn;
      if (game.turn < turn_min) {
        turn_min = game.turn;
      }
      if (turn_max < game.turn) {
        turn_max = game.turn;
      }
    }
    result = msg(
      msg().newline,
      msg().newline,
      msg("Stats: ").span(16),
      msg(win).green.bold,
      msg("/").bold,
      msg(win + lost).bold,
      "   ",
      msg(t((win / numberOfBattels) * 100).fixed(2), "%").bold,
      msg().newline
    );
    result.add(
      msg(
        msg("Antal rundor: "),
        turn_min,
        " < ",
        msg(turn_sum / numberOfBattels).fixed(2),
        " < ",
        turn_max
      ).bold
    );
    break;*/
    default:
      result = msg("");
      print = false;
  }

  if (print) {
    if (false) {
      console.log(result);
    }

    if (true) {
      console.log(result);
    }
  }

  return !print;
}
