#!/usr/bin/env node
"use strict";
import { msg } from "../src/lib/msg";
import yargs from "yargs";
import { aview } from "./aview";

async function main() {
  let result = "";

  const args = yargs
    .command("about", "Information about tihs program.")
    .command("start", "start the server");
  const argv = await args.argv;
  const command = argv._[0];

  if (await aview(argv)) {
    switch (command) {
      case "about":
        result += "This is a game under construction!";
        break;
      case "start":
        result += "This should start a server";
        break;
      default:
        result += "Bad option";
        break;
    }
    console.log(result);
  }
}

main();
