# 0.3 Get test working again

- !Implement a creaturePath parser.
- !Fix option "info" in aview.
- !Fix all option in aview until "calculation".
- !Read Action code and design it.
- Fix bug in mimer-calculation.
- Fix option in aview "selections"
- Fix all option in aview until "damage"
- Read code in battel.
- Base aview on battel mode.
- Fix bug in mimer-calculation with this and property.
- Fix option in aview "turn"
- Fix option in aview "battel"
- Fix option in aview "battelStats"
- Check outher flags options in aview.

# 0.4 Make test with simpel QuickBattel

- Writte an explanation of a battel in documentation file.
- Get logger to work again and start adding log to code.
- Start using errors handling.
- Writte new API för effect and occatoion.
  - Remove create.
- Move effects implmention to world.
- Remove Old folder.
- Review QuickBattel engine.
- Remove all code that is not in use.
- Implment mocha test.
- Make QuickBattel work with test.
- Make creature propertie work, show authors of diffrent resourses.
- Writte tsDoc for all importen funtions.
- Publise mimer-calculation on npm, remove setup.

# 0.5 AI

- Create an AI map.
- Implmentet base AI class from player.
- Implmentet RandomAI.
- Integrate AI to Game
- Integrate AI to QuickGame
- Implment ExpertAI(Using a simnpel json language).
- Add AI handlers to aview.

# 0.6 Server

- Rewritte mimer-game-server
- Writte "Rock paper scissors" as demo in mimer-game-server
- Review Game engine.
- Fix ChoosePath with test.
- Fix all modes with test.
- Update Quickbattel so it works for some mode.
- Create new server from mimer-game-server.
- Make server work with QuickGame.
- Make the game playabel.
- Make an end mode to the game.
- Make a simpel client if mimer-app is not finish.

# 0.7 More game and documentation

- Integrate AI to QuickGame
- Remove logger file and use a package instead.
- Conver documentation to md inside project.
- Add documentation to Game code functions.
- Create BattelGame for battel mode.
- Implmentet a selection mode.
- Get BattelGame to work with selection mode.
- Implmentet a Menu mode.
- Get BattelGame to work with menu mode.
- Let the Game variaons be choosen at start of clent contat.

# 0.8 All Action

- Implment all poosibel effect.
- Read all code and fix it.
- Chagne might to mana, and maby consume mana.

# 0.9 Fix language
